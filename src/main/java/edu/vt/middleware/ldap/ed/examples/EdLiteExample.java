package edu.vt.middleware.ldap.ed.examples;

import java.io.IOException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.vt.middleware.ldap.ed.beans.VirginiaTechPerson;
import edu.vt.middleware.ldap.ed.EdLite;
import org.ldaptive.LdapAttribute;

/**
 * @author Middleware Services
 */
public class EdLiteExample
{

  public static void main(String[] args) throws IOException, Exception
  {
    final EdLite ed = new EdLite();

    //Public People Search Data
    final VirginiaTechPerson vtp = ed.getVirginiaTechPerson("dfisher");

    final Gson gson = new GsonBuilder().
            registerTypeAdapter(LdapAttribute.class,
                    new LdapAttributeSerializer()).setPrettyPrinting().
            create();
    System.out.println("Printing Virginia Tech Person:");
    System.out.println(gson.toJson(vtp));

    // Close connection pool
    ed.close();
  }

}
