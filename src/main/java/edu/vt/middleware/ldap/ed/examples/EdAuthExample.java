package edu.vt.middleware.ldap.ed.examples;

import java.io.IOException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.vt.middleware.ldap.ed.beans.VirginiaTechPerson;
import edu.vt.middleware.ldap.ed.EdAuth;
import edu.vt.middleware.ldap.ed.EdAuthAuthorizationException;
import org.ldaptive.Credential;
import org.ldaptive.LdapAttribute;
import org.ldaptive.LdapEntry;
import org.ldaptive.LdapException;

/**
 * @author Middleware Services
 */
public class EdAuthExample
{

  public static void main(String[] args) throws IOException, Exception
  {
    /**
     * Virginia Tech PID of the Person
     *
     */
    final String authId = "ememisya";

    /**
     * Password of the Person
     *
     * https://www.owasp.org/index.php/Use_of_hard-coded_password
     *
     */
    String evilPlainTextHardCodedPasswordYouShouldNeverDo = "Don't";

    /**
     * The EdAuth instance. You may switch contexts with the constructor as
     * shown below: For Develop: EdAuth(DirectoryEnv.DEV);
     *
     */
    EdAuth ed = new EdAuth();

    /**
     * authorizationExpression is a Spring SpEL expression which uses the
     * methods hasAttributeValue(attributeName, value) and
     * hasAttribute(attributeName) in any provided context for authorization.
     *
     * Example #1: If the user has any groupMemberships, and is an active VT
     * person, the expression would be as follows:
     *
     * "hasAttribute('groupMembership') &&
     * hasAttributeValue('eduPersonAffiliation', 'VT-ACTIVE-MEMBER')"
     *
     * Example #2: If the user is a member of 'middleware.staff' group or a
     * VT-FACULTY affiliate, the expression would be as follows:
     *
     * "hasAttributeValue('groupMembership',
     * 'uugid=middleware.staff,ou=Groups,dc=vt,dc=edu') ||
     * hasAttributeValue('eduPersonAffiliation', 'VT-ACTIVE-MEMBER')"
     *
     *
     */
    final String authorizationExpression = "hasAttributeValue('groupMembership',"
            + "'uugid=middleware.staff,ou=Groups,dc=vt,dc=edu')";

    System.out.print("Attempting to authenticate user...");

    final LdapEntry authenticationResult;

    //3.2.0 returns LdapEntry for authorization as a second step.
    try {
      authenticationResult = ed.authenticate(authId, new Credential(evilPlainTextHardCodedPasswordYouShouldNeverDo));

      //No exceptions means user was authenticated.
      System.out.println("User authenticated.");
    } catch (LdapException authenticationException) {
      //An LDAP binding exception has occured.
      System.out.println("Could not authenticate user.");
      throw authenticationException;
    }

    //3.2.0 returns LdapEntry for authorization as a second step.
    try {
      ed.authorize(authenticationResult, authorizationExpression);
      System.out.println("User authorized.");
    } catch (EdAuthAuthorizationException authorizationException) {
      //User was not authorized.
      System.out.println("Could not authorize user.");
      throw authorizationException;
    }

    final Gson gson = new GsonBuilder().
            registerTypeAdapter(LdapAttribute.class,
                    new LdapAttributeSerializer()).setPrettyPrinting().
            create();

    final VirginiaTechPerson vtp = ed.getVirginiaTechPerson(authenticationResult);

    System.out.println("Printing Virginia Tech Person '" + authId + "':");
    System.out.println(gson.toJson(vtp));

  }

}
