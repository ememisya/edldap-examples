package edu.vt.middleware.ldap.ed.examples;

import java.io.IOException;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.vt.middleware.ldap.ed.beans.VirginiaTechEntitlement;
import edu.vt.middleware.ldap.ed.beans.VirginiaTechPerson;
import edu.vt.middleware.ldap.ed.EdId;
import org.ldaptive.LdapAttribute;

/**
 * @author Middleware Services
 */
public class EdIdExample
{

  public static void main(String[] args) throws IOException, Exception
  {
    final EdId ed = new EdId();

    final VirginiaTechPerson vtp = ed.getVirginiaTechPerson("ememisya");

    final Gson gson = new GsonBuilder().
            registerTypeAdapter(LdapAttribute.class,
                    new LdapAttributeSerializer()).setPrettyPrinting().
            create();

    System.out.println("Printing Virginia Tech Person 'ememisya':");
    System.out.println(gson.toJson(vtp));

    final List<VirginiaTechEntitlement> vtel
            = ed.getVirginiaTechEntitlementsByViewerService("middleware-test");

    for (VirginiaTechEntitlement vte : vtel) {
      System.out.println("Printing Virginia Tech Entitlement:");
      System.out.println(gson.toJson(vte));
    }
    // Close connection pool
    ed.close();
  }

}
