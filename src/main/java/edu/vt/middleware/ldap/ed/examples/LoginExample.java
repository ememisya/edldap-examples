package edu.vt.middleware.ldap.ed.examples;

import java.io.IOException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.vt.middleware.ldap.ed.DuoFactor;
import edu.vt.middleware.ldap.ed.beans.VirginiaTechPerson;
import edu.vt.middleware.ldap.ed.Login;
import org.ldaptive.Credential;
import org.ldaptive.LdapAttribute;
import org.ldaptive.LdapEntry;
import org.ldaptive.LdapException;

/**
 * @author Middleware Services
 */
public class LoginExample
{

  public static void main(String[] args) throws IOException, Exception
  {
    /**
     * Virginia Tech PID of the Person
     */
    final String authId = "ememisya";

    /**
     * See:
     *
     * https://www.owasp.org/index.php/Use_of_hard-coded_password
     */
    final String evilPlainTextHardCodedPasswordYouShouldNeverDo = "Don't";

    /**
     * Login instance
     */
    final Login edmfa = new Login();

    /**
     * Login authentication result
     */
    final LdapEntry authenticationResult;

    //3.2.0 returns LdapEntry for authorization as a second step.
    try {
      System.out.println("Attempting login proxy auth, be prepared to provide your second factor...");
      //We select PUSH authentication method for this example.  If unspecified default factor will be used.
      authenticationResult = edmfa.authenticate(authId,
              new Credential(
                      evilPlainTextHardCodedPasswordYouShouldNeverDo),
              new DuoFactor(DuoFactor.Type.PUSH));
      //No exceptions means user was authenticated.
      System.out.println("Authenticated user.");
    } catch (LdapException authenticationException) {
      //An LDAP binding exception has occured.
      System.out.println("Could not authenticate user.");
      throw authenticationException;
    }

    final Gson gson = new GsonBuilder().
            registerTypeAdapter(LdapAttribute.class,
                    new LdapAttributeSerializer()).setPrettyPrinting().
            create();

    final VirginiaTechPerson vtp = edmfa.getVirginiaTechPerson(authenticationResult);

    System.out.println("Printing Virginia Tech Person '" + authId + "':");
    System.out.println(gson.toJson(vtp));
  }

}
