package edu.vt.middleware.ldap.ed.examples;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import org.ldaptive.LdapAttribute;

/**
 * @author Middleware Services
 */
public class LdapAttributeSerializer implements JsonSerializer<LdapAttribute>
{

  @Override
  public JsonElement serialize(LdapAttribute src, Type typeOfSrc, JsonSerializationContext context)
  {
    return new JsonPrimitive(src.getStringValue());
  }

}
