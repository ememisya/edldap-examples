This project is intended as a demonstration to show sample usage of the EDLdap library.

# See [LoginExample](https://git.it.vt.edu/ememisya/edldap-examples/blob/master/src/main/java/edu/vt/middleware/ldap/ed/examples/LoginExample.java) for an MFA demonstration.

# You may execute individual sources as follows (for EdLiteExample):

```
mvn -Dexec.args="-classpath %classpath edu.vt.middleware.ldap.ed.examples.EdLiteExample" -Dexec.executable=java -Dexec.classpathScope=runtime org.codehaus.mojo:exec-maven-plugin:1.2.1:exec
```

# For EdIdExample you must configure the following file: src/main/resources/edldap.properties
